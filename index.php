<?php

    $pageDetails = [
        'title' => 'AutoMedic',
        'description' => 'From factory recommended maintenance to complete auto repair, our auto service experts can help keep your car on the road longer.'
    ];

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');
?>

<noscript>
    <div class="alert alert-danger animated infinite flash" role="alert">
    Uh, oh.. No javascript present :(
    </div>
</noscript>

<div class="jumbotron jumbotron-fluid text-center">
    <div class="container">
    <h1 class="display-3"><?= $pageDetails['title'] ?></h1>
    <p class="lead"><?= $pageDetails['description'] ?></p>
    </div>
</div>

<?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>