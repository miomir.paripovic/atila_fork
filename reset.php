<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    // Check for tokens
    $selector = filter_input(INPUT_GET, 'selector');
    $validator = filter_input(INPUT_GET, 'validator');

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form

        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'selector' => $selector,
            'validator' => $validator,
            'password' => trim($_POST['password']),
            'confirm_password' => trim($_POST['confirm_password']),
            'password_err' => '',
            'confirm_password_err' => ''
        ];

        // Validate password
        if(empty($data['password'])) {
            $data['password_err'] = 'Please enter password';
        } elseif(strlen($data['password']) < 6) {
            $data['password_err'] = 'Password must be at least 6 characters';
        }

        // Validate confirm password
        if(empty($data['confirm_password'])) {
            $data['confirm_password_err'] = 'Please confirm password';
        } else {
            if($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }
        }

        // Get tokens
        $results = verifyTokens($pdo, $data);

        if (empty($results)) {
            $msg->error('There was an error processing your request.', '/login.php');
        }

        $calc = hash('sha256', hex2bin($validator));

        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

        // Validate tokens
        if ( hash_equals( $calc, $results->token ) )  {
            $user = findUserByEmail($pdo, $results->email);
            if($user[0] === true) {
                // Update password
                updatePassword($pdo, $data['password'], $user[1]->id);

                // Delete any existing password reset for this user
                deleteExistingToken($pdo, $user[1]->email);
                $msg->success('Password updated successfully. You can now login with your new credentials.');
                redirect('/login.php');

            } else {
                $msg->error('There was an error processing your request.', '/login.php');
            }
        }

    }

    // Init data
    $data = [
        'password' => '',
        'confirm_password' => '',
        'password_err' => '',
        'confirm_password_err' => ''
    ];

    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/header.php');

    if ( false !== ctype_xdigit( $selector ) && false !== ctype_xdigit( $validator ) ) : ?>
    
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5 mb-5">
                <h2>Create A New Password</h2>
                <p>Please fill out this form to create your new password.</p>
                <!-- <form action="reset_process.php" method="post" class="needs-validation" novalidate> -->
                <form action="<?= basename($_SERVER['REQUEST_URI']); ?>" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="password">Password: <sup>*</sup></label>
                        <input type="password" name="password" class="form-control form-control <?= (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['password'] ?>">
                        <span class="invalid-feedback"><?= $data['password_err'] ?></span>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password: <sup>*</sup></label>
                        <input type="password" name="confirm_password" class="form-control form-control <?= (!empty($data['confirm_password_err'])) ? 'is-invalid' : ''; ?>" value="<?= $data['confirm_password'] ?>">
                        <span class="invalid-feedback"><?= $data['confirm_password_err'] ?></span>
                    </div>

                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col">
                            <input type="submit" value="Create New Password" class="btn btn-block main-color-bg" id="register_button">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
        
    <?php else : ?>

    <div class="alert alert-danger mt-5 mb-5" role="alert">
        <h4 class="alert-heading">Invalid Url!</h4>
        <p>Please use the link that has been sent to your email.</p>
        <hr>
        <small><a href="/" class="alert-link">Back to homepage</a></small>
    </div>

    <?php endif ?>

    <?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>