<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT); // Set job id variable
        
        $pageDetails = [
            'tagline' => 'job details',
            'title' => 'Job Details'
        ];
        
        $job = getSingleJob($pdo, $id);
        // Check if job exists
        if(!$job) {
            $msg->error('Job does not exist.', '/admin/jobs/');
        } else {
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
            require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
            ?>
            <main>
                <div class="container">
                    <div class="row">
                        <!-- Sidebar -->
                        <div class="col-md-3 d-none d-md-block">
                            <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                        </div>
                        <div class="col-md-9">
                            <!-- Job details-->
                            <div class="card">
                                <div class="card-header main-color-bg"><i class="fas fa-wrench"></i> Job details</div>
                                    <div class="card-body">
                                        <!-- Show flash message -->
                                        <?php $msg->display() ?>
                                        <div class="d-flex justify-content-between">
                                            <div>
                                                <h2>
                                                    <small class="text-muted">Job number</small>
                                                    <strong><?php echo 'AM-'.$job[0]->jobId.'-'.date("y"); ?></strong>
                                                </h2>
                                            </div>
                                            <div>
                                                <a href="/admin/jobs/edit/<?= $job[0]->jobId ?>" class="btn btn-main">Edit Job</a>
                                            </div>
                                        </div>
                                        <!-- Details card -->
                                        <div class="container-fluid bg-light mt-2 pb-3 border">
                                            <div class="mt-3">
                                                <div class="row" style="line-height: 1.8">
                                                    <div class="col-md-3"><strong>Status:</strong></div>
                                                    <div class="col-md-9">
                                                        <button class="btn btn-main btn-sm disabled"><?= ucwords($job[0]->jobStatus) ?></button>
                                                    </div>

                                                    <div class="col-md-3"><strong>Total Price:</strong></div>
                                                    <div class="col-md-9"><?= (is_null($job[0]->total_price)) ? '<span class="text-muted">---</span>' : $job[0]->total_price; ?></div>

                                                    <div class="col-md-3"><strong>Total Time Needed:</strong></div>
                                                    <div class="col-md-9"><?= (is_null($job[0]->total_time)) ? '<span class="text-muted">---</span>' : $job[0]->total_time; ?></div>

                                                    <div class="col-md-3"><strong>Date Created:</strong></div>
                                                    <div class="col-md-9"><?= date("m/d/Y h:i A", strtotime($job[0]->dateCreated)) ?></div>

                                                    <div class="col-md-3"><strong>Last Updated:</strong></div>
                                                    <div class="col-md-9"><?= date("m/d/Y h:i A", strtotime($job[0]->lastUpdated)) ?></div>

                                                    <div class="col-md-3"><strong>Services:</strong></div>
                                                    <div class="col-md-9">
                                                    <?php foreach($job[1] as $service) : ?>
                                                        <button class="btn btn-secondary btn-sm disabled my-1"><?= $service->services ?></button>
                                                    <?php endforeach; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row px-3 pt-3">
                                            <!-- Car info card -->
                                            <div class="col-lg mr-lg-2 p-3 bg-light border">
                                                <div class="d-flex justify-content-between">
                                                    <h5><strong>Car</strong></h5>
                                                    <a href="/admin/cars/edit/<?= $job[0]->carId ?>" class="btn btn-secondary btn-sm">Edit Car</a>
                                                </div>
                                                <hr>
                                                <div class="row" style="line-height: 1.8">
                                                    <div class="col-md-6 col-lg-5"><strong>Plate #:</strong></div>
                                                    <div class="col-md-6 col-lg-7"><?= $job[0]->plateNumber ?></div>

                                                    <div class="col-md-6 col-lg-5"><strong>Brand:</strong></div>
                                                    <div class="col-md-6 col-lg-7"><?= ucwords($job[0]->carBrand) ?></div>

                                                    <div class="col-md-6 col-lg-5"><strong>Model:</strong></div>
                                                    <div class="col-md-6 col-lg-7"><?= strtoupper($job[0]->carModel) ?></div>

                                                    <div class="col-md-6 col-lg-5"><strong>Year:</strong></div>
                                                    <div class="col-md-6 col-lg-7"><?= $job[0]->carYear ?></div>
                                                </div>
                                            </div>

                                            <!-- Customer info card -->
                                            <div class="col-lg ml-lg-2 mt-3 mt-lg-0 p-3 bg-light border">
                                                <div class="d-flex justify-content-between">
                                                <h5><strong>Customer</strong></h5>
                                                <a href="/admin/users/edit/<?= $job[0]->userId ?>" class="btn btn-secondary btn-sm">Edit Customer</a>
                                                </div>
                                                <hr>
                                                <div class="row" style="line-height: 1.8">
                                                    <div class="col-lg-3"><strong>Name:</strong></div>
                                                    <div class="col-lg-9"><?= $job[0]->userName ?></div>

                                                    <div class="col-lg-3"><strong>Email:</strong></div>
                                                    <div class="col-lg-9">---</div>

                                                    <div class="col-lg-3"><strong>Phone #:</strong></div>
                                                    <div class="col-lg-9">---</div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Notes card -->

                                        <a class="btn btn-secondary btn-block mt-3" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                            View Job Notes
                                        </a>
                                        <div class="collapse" id="collapseExample">
                                        <div class="card card-body">
                                        <?= nl2br($job[0]->notes) ?>
                                        </div>
                                        </div>

                                    </div> <!-- .card-body end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>

            <?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

    <?php }} else {
        $msg->error('You aren\'t really allowed to do that.', '/admin/jobs/');
    }
?>