<?php
    $pageDetails = [
        'tagline' => 'manage jobs',
        'title' => 'Manage Jobs'
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
    $jobs = getAllJobs($pdo);
?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Jobs overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="fas fa-wrench"></i> Jobs overview</div>
                        <div class="card-body">
                            <!-- Show flash message -->
                            <?php $msg->display() ?>
                            <table id="jobs-table" class="table table-striped table-responsive-md">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col" class="text-center">Name</th>
                                <th scope="col" class="text-center">Car</th>
                                <th scope="col" class="text-center">Status</th>
                                <th scope="col" class="text-center">Notes</th>
                                <th scope="col" class="text-center">Date Created</th>
                                <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php foreach($jobs[1] as $job) : ?>
                                <tr>
                                <td class="align-middle"><?= $job->userName ?></td>
                                <td class="align-middle"><?= $job->plateNumber ?></td>
                                <td class="align-middle"><?= $job->jobStatus ?></td>
                                <td class="align-middle text-center">
                                    <?php if(is_null($job->notes) || empty($job->notes)) : ?>
                                        <div class="text-muted"><small><i class="fas fa-times"></i></small></div>
                                    <?php else : ?>
                                    <button type="button" class="btn btn-sm" data-toggle="modal" data-target="#ModalLong<?= $job->JobId ?>">View notes</button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ModalLong<?= $job->JobId ?>" tabindex="-1" role="dialog" aria-labelledby="ModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="p-3 my-3">
                                                    <?= nl2br($job->notes) ?>
                                                    </div>
                                                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </td>
                                <td class="align-middle"><small><?= date( "m/d/Y h:i A", strtotime($job->dateCreated)) ?></small></td>
                                <td class="align-middle">
                                    <a href="/admin/jobs/show/<?= $job->JobId ?>" class="btn btn-sm btn-secondary"><i class="far fa-eye"></i></a>
                                    <a href="/admin/jobs/edit/<?= $job->JobId ?>" class="btn btn-sm btn-main"><i class="fas fa-pen"></i></a>
                                    <a href="/admin/jobs/delete/<?= $job->JobId ?>" class="btn btn-sm btn-danger confirm-delete"><i class="far fa-trash-alt"></i></a>
                                </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
    $('#jobs-table').DataTable();
    });
</script>