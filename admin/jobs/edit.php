<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
 
    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);        
    } else {
        redirect('/admin/jobs/');
    }    

    // Meta/SEO data for the current page
    $pageDetails = [
        'tagline' => 'manage jobs',
        'title' => 'Edit Job'
    ];
    // Include in header
    $headerLinks = array(
        '<link rel="stylesheet" href="/css/bootstrap-multiselect.css" type="text/css"/>'
    );
    // Include in footer
    $footerLinks = array(
        '<script type="text/javascript" src="/js/bootstrap-multiselect.js"></script>'
    );

    // Grab data required for the form

    $services = getServices($pdo);
    $status = getAllStatus($pdo);
    $job = getSingleJob($pdo, $id);
    var_dump($job);

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form
        
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Init data
        $data = [
            'status' => trim($_POST['status']),
            'total_price' => trim($_POST['total_price']),
            'total_time' => trim($_POST['total_time']),
            'car' => trim($_POST['car']),
            // array services already in dbase
            'services' => $_POST['services'],
            'notes' => trim($_POST['description']),
            'car_err' => '',
            'total_price_err' => ''
        ];        

    // Make sure errors are empty
    if(empty($data['car_err']) && empty($data['total_price_err'])) {
        // Update user
        // dd(array($data, $id));
        if(adminUpdateJob($pdo, $data, $id)) {
            $msg->success('Job updated.', '/admin/jobs/');
        } else {
            die('Something went wrong');
        }
    }

    } else {
        // Init data
        $data = [
            'status' => $job[0]->status_id,
            'total_price' => '',
            'total_time' => '',
            'car' => '',
            'services' => '',
            'notes' => '',
            'car_err' => '',
            'total_price_err' => ''
        ];
    }

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');

    // dd(array($services, $job[1]));
?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-edit"></i> Edit job</div>
                        <div class="card-body">
                        <form action="/admin/jobs/edit/<?= $id ?>" method="POST" class="pr-4 pl-4 needs-validation" id="edit-job-form" novalidate>
                            <div class="form-group row">
                                <label for="status" class="col-sm-4 col-form-label">Status:</label>
                                <div class="col-sm-8">
                                <select name="status" class="form-control <?= (!empty($data_sp['status_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->jobStatus ?>">
                                    <?php foreach($status as $name) : ?>
                                    <option value="<?= $name->id ?>"><?= $name->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Total Price (Euros):</label>
                                <div class="col-sm-8">
                                    <input type="text" name="total_price" class="form-control <?= (!empty($data['total_price_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->total_price ?>" id="total_price_field">
                                    <span class="invalid-feedback"><?= $data['total_price_err'] ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Total Time Needed (minutes):</label>
                                <div class="col-sm-8">
                                    <input name="total_time" class="form-control <?= (!empty($data_sp['total_time_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->total_time ?>">           
                                    <span class="invalid-feedback"><?= $data['total_time_err'] ?></span>                             
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label">Car:</label>
                                <div class="col-sm-8">
                                    <input type="text" name="car" class="form-control <?= (!empty($data['car_err'])) ? 'is-invalid' : ''; ?>" value="<?= $job[0]->plateNumber ?>" id="car_field">
                                    <span class="invalid-feedback"><?= $data['car_err'] ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="services" class="col-sm-4 col-form-label">Services:</label>
                                <div class="col-sm-8">
                                <select name="services[]" multiple id="select-services" class="form-control <?= (!empty($data_sp['service_err'])) ? 'is-invalid' : ''; ?>">
                                <?php for($i=0; $i < count($services); $i++) {
                                        echo '<option value="'.$services[$i]->id.'"';
                                        for($j=0; $j < count($job[1]); $j++) {
                                            if ($services[$i]->id ==  $job[1][$j]->serviceId) {
                                                echo ' selected';
                                            }
                                        }
                                        echo '>'.$services[$i]->name.'</option>';
                                }; ?>
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="description" rows="5" placeholder="Tell us as much as you can about the problem.."><?= $job[0]->notes ?></textarea>
                            </div>
                            <div class="row">
                                <div class="col"><input type="submit" value="Submit" class="btn btn-block main-color-bg disabled"></div>
                                <div class="col"><a class="btn btn-secondary btn-block" href="/admin/jobs/">Back</a></div>
                            </div>
                        </form>
                        </div>
                </div>
            </div>
        </div>
    </main>

<?php include ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<script>
    $('#select-services').multiselect({
        buttonWidth: '100%',
        maxHeight: 400,
        
    });
</script>