<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    if(isset($_GET['id']) && !empty($_GET['id']) && CTYPE_DIGIT($_GET['id'])) {
        $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
        if(adminDeleteJob($pdo, $id)) {
            $msg->success('Job deleted.', '/admin/jobs/');
        } else {
            die('Something went wrong!');
        }
    } else {
        redirect('/admin/jobs/');
    }

?>