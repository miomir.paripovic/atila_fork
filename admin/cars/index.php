<?php
    $pageDetails = [
        'tagline' => 'manage cars',
        'title' => 'Manage Cars'
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/navigation.php');
    $cars = getAllCars($pdo);
?>
    <main>
        <div class="container">
            
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/admin/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <!-- Users overview-->
                <div class="card">
                    <div class="card-header main-color-bg"><i class="far fa-user-circle"></i> Users overview</div>
                        <div class="card-body">
                            <!-- Show flash message -->
                            <?php $msg->display() ?>
                            <table id="cars-table" class="table table-striped table-responsive-md">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col" class="text-center">Name</th>
                                <th scope="col" class="text-center">Plate Number</th>
                                <th scope="col" class="text-center">Brand</th>
                                <th scope="col" class="text-center">Model</th>
                                <th scope="col" class="text-center">Year</th>
                                <th scope="col" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            <?php foreach($cars as $car) : ?>                            
                                <tr>
                                <td class="align-middle text-center"><?= $car->name ?></td>
                                <td class="align-middle text-center"><?= $car->plate_number ?></td>
                                <td class="align-middle text-center"><?= $car->brand ?></td>
                                <td class="align-middle text-center"><?= $car->model ?></td>
                                <td class="align-middle text-center"><?= $car->year ?></td>                   
                                
                                <td class="align-middle text-center">
                                   <a href="/admin/jobs/show/<?= $car->carsId ?>" class="btn btn-sm btn-secondary"><i class="far fa-eye"></i></a>
                                   <a href="/admin/cars/edit/<?= $car->carsId ?>"class="btn btn-sm btn-main"><i class="fas fa-pen"></i></a>
                                   <a href="/admin/cars/delete/<?= $car->carsId ?>"class="btn btn-sm btn-danger confirm-delete"><i class="far fa-trash-alt"></i></a>
                                </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </main>

<div class="container">
<?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {
    $('#cars-table').DataTable();
} );
</script>