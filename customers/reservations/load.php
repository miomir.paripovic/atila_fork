<?php     
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    if(isset($_COOKIE['reservations'])) {        
        //load.php
        //$connect = new PDO('mysql:host=localhost; dbname=automedic', 'root', '123456');
        $data = array();
        $query = "SELECT * FROM reservations ORDER BY id";
        $statement = $connect->prepare($query);
        $statement->execute();
        $result = $statement->fetchAll();

        foreach($result as $row)
        {
            $data[] = array(
                'id'   => $row["id"],
                'cars_id' => $row["cars_id"],
                'start' => $row["start_date"],
                'end' => $row["end_date"]        
            );
        }
        echo json_encode($data);        
    }
?>