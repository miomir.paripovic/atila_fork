<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    //update.php
    //$connect = new PDO('mysql:host=localhost;dbname=automedic', 'root', '123456');
    if(isset($_POST["id"])) {
        $query = "
            UPDATE app_table 
            SET cars_id=:cars_i, start_date=:start_date, end_date=:end_date 
            WHERE id=:id
            ";
            $statement = $connect->prepare($query);
            $statement->execute(
            array(    
                ':start_date' => $_POST['start'],
                ':end_date' => $_POST['end'],
                ':cars_id' => $_POST['cars_id'],
                ':id'   => $_POST['id']
            )
        );
}
?>
