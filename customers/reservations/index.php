<?php
    $pageDetails = [
        'tagline' => 'reservations',
        'title' => 'Reservation',
        'breadcrumb' => array('Reservation')
    ];
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/reservations/header.php');
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/navigation.php');

    // Check if the visitor comes by filling the form
    if(!isset($_COOKIE['reservations'])) {
        redirect('/customers/');
    }
    
    //setcookie('reservations', $_SESSION['car_id'] . "." . '60', time()+3600, '/customers/');    
    var_dump($_COOKIE['reservations']);
    var_dump($_SESSION['car_id']);
    var_dump($_SESSION['user_id']);
    var_dump($_SESSION['duration']);
    var_dump('lastId', $_SESSION['lastId']);
    
//    require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/reservations/header.php');
  
?>
    <main>
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3 d-none d-md-block">
                    <?php require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/customers/sidebar.php'); ?>
                </div>
                <div class="col-md-9">
                <div class="d-none d-md-block">
                    <?= $msg->display(); ?>
                </div>
                    <!-- Calendar -->
                    <div class="card mb-4">
                        <div class="card-header main-color-bg">Calendar</div>
                            <div class="card-body">
                                <div id="calendar" class="p-3"></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Overlap Model for Calendar -->
    <div class="modal fade" id="cal-overlap-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-body">
            <div class="mb-1">
                <button type="button" class="close-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <h3 class="text-center mb-5"><i class="fas fa-exclamation-circle text-danger"></i> Overlap!</h3>
            <p class="mb-4 text-center">Please choose another time for schedule overlap is not allowed.</p>            
        </div>
        </div>
    </div>
    </div>

    <style>
        body {
            margin: 40px 10px;
            padding: 0;
            font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 800px;
            min-width: 375px;
            margin: 0 auto;
        }

        .hoverEffect {
            font-size: 29px;
            position: absolute;
            margin: 30px 55px;
            cursor: pointer;
        }

        #calendar td.fc-header-center {
    text-align: right !important;
}
#calendar table.fc-header {
    font-size: 13px !important;
}
	</style>

<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Call .js Files -->
<script src="/js/reservations_cal.js"></script>

<!-- Load No Car Registered Modal -->
<?php
    if(!userHasCar($pdo, $_SESSION['user_id'])) {
        echo '<script src="/js/no_car.js"></script>';
    };
?>