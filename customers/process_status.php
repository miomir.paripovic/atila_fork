<?php

    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    // Check if $_POST is set
    if (empty($_POST['plate_number'])) {
    echo "Something went wrong";
    exit;
    }

    // Init data
    $data = [
        'plate_number' => strtoupper(preg_replace('/[^a-zA-Z0-9]/', '', $_POST['plate_number'])),
        'user_id' => $_SESSION['user_id']
    ];

    $carDetails = itsUsersCar($pdo, $data);

    // Check if user owns the car
    if($carDetails[0]) {
        $carId = $carDetails[1]->id;

        // Get status
        $carStatus = getVehicleStatus($pdo, $data, $carId);

        // Check if the car is on service
        if(!$carStatus) {
            echo '
            <div class="alert alert-info animated flash" role="alert">
            The car with that plate number isn\'t on service.
            </div>
            ';
        } else { ?>

            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Car found!</h4>
            <hr>
            <p><strong>Plate Number:</strong> <?= $data['plate_number'] ?></p>
            <p><strong>Current Status:</strong> <?= ucwords($carStatus->statusName) ?></p>
            <p><strong>Current Service:</strong> <?= $carStatus->serviceName ?></p>
            <p><strong>Current Time:</strong> <?= date('m/d/Y h:i:s a', time()) ?></p>
            <hr>
            <p class="mb-0"><?= json_encode($carStatus) ?></p>
            </div>

            
        <?php }
    } else {
        echo '
        <div class="alert alert-danger animated flash" role="alert">
        You don\'t have a car registered with that plate number. Try again.
        </div>
        ';
    }