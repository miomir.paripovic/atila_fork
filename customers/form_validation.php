<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');

    $data_nv = [
        'plate_number' => '',
        'brand' => '',
        'model' => '',
        'year' => '',
        'user_id' => '',
        'show-modal' => '',
        'plate_number_err' => '',
        'brand_err' => '',
        'model_err' => '',
        'year_err' => ''
    ];

    $data_sp = [
        'plate_number' => '',
        'services' => '',
        'description' => '',
        'user_id' => '',
        'show-modal' => '',
        'plate_number_err' => '',
        'services_err' => ''
    ];

    // Check for POST method
    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        // process form
        
        // Sanitize POST data
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        // Check if it's the New Vehicle form
        if($_POST['form_name'] == 'new-vehicle-form') {

            // Init data
            $data_nv = [
                'plate_number' => strtoupper(preg_replace('/[^a-zA-Z0-9]/', '', $_POST['plate_number'])),
                'brand' => trim($_POST['brand']),
                'model' => trim($_POST['model']),
                'year' => trim($_POST['year']),
                'user_id' => $_SESSION['user_id'],
                'show-modal' => '',
                'plate_number_err' => '',
                'brand_err' => '',
                'model_err' => '',
                'year_err' => ''
            ];

            // Validate plate number
            if(empty($data_nv['plate_number'])) {
                $data_nv['plate_number_err'] = 'Please enter plate number';
            } else {
                //Check plate number
                if(findPlateNumber($pdo, $data_nv['plate_number'])) {
                    $data_nv['plate_number_err'] = 'Plate number already registered';
                }
            }

            // Validate brand
            if(empty($data_nv['brand'])) {
                $data_nv['brand_err'] = 'Please enter brand';
            }

            // Validate model
            if(empty($data_nv['model'])) {
                $data_nv['model_err'] = 'Please enter model';
            }

            // Validate year
            if(empty($data_nv['year'])) {
                $data_nv['year_err'] = 'Please enter year';
            }

            if(empty($data_nv['plate_number_err']) && empty($data_nv['brand_err']) && empty($data_nv['model_err'])) {
                // Add vehicle
                if(addNewVehicle($pdo, $data_nv)) {
                    $msg->success('Vehicle successfully added.', '/customers/');
                } else {
                    $msg->error('Something went wrong. Please try again.');
                }
            } else {
                $data_nv['show-modal'] = "<script>$('#new-vehicle-modal').modal('show')</script>";
            }

        // Check if it's the Submit Problem form
        } elseif($_POST['form_name'] == 'submit-problem-form') {
            // Init data
            $data_sp = [
                'car_id' => $_POST['car_id'],
                'services' => $_POST['services'],
                'description' => trim($_POST['description']),
                'user_id' => $_SESSION['user_id'],
                'show-modal' => '',
                'car_id_err' => '',
                'services_err' => ''
            ];

            $_SESSION['car_id'] = $_POST['car_id'];

            if(empty($data_sp['car_id'])) {
                $data_sp['car_id_err'] = 'Please choose at least one car';
            }

            if(empty($data_sp['services'])) {
                $data_sp['services_err'] = 'Please choose at least one service';
            }

            if(empty($data_sp['car_id_err']) && empty($data_sp['brand_err']) && empty($data_sp['model_err'])) {
                // Create problem
                if(submitProblem($pdo, $data_sp)) {
                    // read duration from db and place it into cookie (fetchall last one ???)
                    //var_dump(addDurationPerJob($pdo, (int)$_SESSION['lastId']));
                    addDurationPerJob($pdo, (int)$_SESSION['lastId']);
                    addPricesPerJob($pdo, (int)$_SESSION['lastId']);
                    //var_dump(addPricesPerJob($pdo, (int)$_SESSION['lastId']));
                    $duration = (string)getDuration($pdo, $_SESSION['lastId']);                    
                    $_SESSION['duration'] = $duration;
                    setcookie('reservations', (string)$_SESSION['car_id']  . '.' . $duration, time() + 3600, '/customers');
                    $msg->info('Thanks, details have been saved! Please book an appointment below to finish your submission.', '/customers/reservations/');
                } else {
                    $msg->error('Something went wrong. Please try again.');
                }
            } else {
                $data_sp['show-modal'] = "<script>$('#submit-problem-modal').modal('show')</script>";
            }
        }
    }
?>