-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2018 at 11:31 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `automedic`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(11) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `brand` varchar(60) NOT NULL,
  `model` varchar(90) NOT NULL,
  `year` year(4) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `plate_number`, `brand`, `model`, `year`, `user_id`) VALUES
(1, 'SU123AB', 'Fiat', '500c', 2017, 2),
(2, 'SU456CD', 'Tesla', 'Model X', 2018, 4),
(3, 'SU789EF', 'VW', 'Bora', 2003, 5),
(4, 'SU111AA', 'fiat', 'xsd300', 0000, 4),
(5, 'SU609OQ', 'jaguar', 'i-pace', 2018, 4);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reservation_id` int(11) NOT NULL,
  `car_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `status_id` tinyint(1) NOT NULL DEFAULT '1',
  `note` text,
  `total_price` float DEFAULT NULL,
  `total_time` time DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `user_id`, `reservation_id`, `car_id`, `service_id`, `status_id`, `note`, `total_price`, `total_time`, `created_at`, `updated_at`) VALUES
(3, 4, 1, 2, 2, 1, NULL, NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:54'),
(4, 4, 0, 4, 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris bibendum odio id ultrices condimentum. Quisque ac nunc faucibus, lobortis tortor sed, ullamcorper nisl. Nunc pharetra egestas libero, at porttitor orci aliquet nec. Integer volutpat ultrices luctus. Maecenas maximus non orci at tincidunt. Aenean malesuada risus convallis lectus sagittis, ut fringilla libero fermentum. Aenean consectetur elit sed ipsum venenatis efficitur. Suspendisse aliquet a enim at finibus.\r\n\r\nMorbi rhoncus elit sit amet felis molestie gravida. Phasellus lacinia, turpis quis rhoncus facilisis, dui orci vulputate tellus, et porta nulla ante vel ligula. Nunc quis leo et dui aliquam euismod. Suspendisse felis est, consectetur ac sapien ut, sollicitudin fringilla magna. Sed eleifend quis erat vitae molestie. Duis in arcu eu ligula fermentum placerat. Duis iaculis odio dolor, nec fringilla nunc malesuada eu. Morbi ac ex est. Ut et tellus at odio porta cursus. Vestibulum et turpis a quam sollicitudin sagittis malesuada id lectus. Nullam porttitor, augue et varius dictum, eros nibh egestas orci, ac consequat dolor enim a mi. Praesent dapibus nunc a lacus vestibulum vulputate. Duis ac mattis nisi. Praesent maximus eget metus a pellentesque.\r\n\r\nVivamus sed sapien vulputate, auctor dui maximus, pharetra ligula. Aliquam erat volutpat. Sed leo ligula, commodo non viverra quis, ullamcorper et ipsum. Ut vestibulum ac tortor nec molestie. Suspendisse dolor arcu, condimentum sit amet scelerisque a, blandit vitae justo. Maecenas gravida fringilla turpis, sed efficitur justo accumsan a. Aliquam ullamcorper eros et maximus mattis. Aenean eu neque ullamcorper, vehicula nisl eu, ornare odio. Integer sit amet dolor maximus, hendrerit diam vel, consequat nibh. Aenean sed gravida nisi, sit amet tincidunt justo.', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:23:58'),
(5, 4, 0, 5, 0, 1, 'fdfdfdfdf', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(6, 4, 0, 5, 0, 1, 'test', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(7, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(8, 4, 0, 5, 0, 1, 'sdsd', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(10, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(11, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(12, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(13, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(14, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(15, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(16, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(17, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(18, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(19, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(20, 4, 0, 5, 0, 1, '', NULL, NULL, '2018-07-16 12:04:57', '2018-07-19 01:22:47'),
(21, 4, 0, 2, 0, 1, '', NULL, NULL, '2018-07-19 11:29:40', '2018-07-19 11:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `job_service`
--

CREATE TABLE `job_service` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_service`
--

INSERT INTO `job_service` (`id`, `job_id`, `service_id`) VALUES
(1, 4, 1),
(2, 4, 4),
(3, 4, 5),
(4, 4, 8),
(5, 5, 2),
(6, 6, 2),
(7, 6, 3),
(8, 7, 4),
(9, 8, 2),
(10, 8, 3),
(11, 9, 2),
(12, 9, 3),
(13, 10, 3),
(14, 10, 4),
(15, 11, 4),
(16, 12, 3),
(17, 13, 3),
(18, 14, 3),
(19, 15, 4),
(20, 15, 5),
(21, 16, 1),
(22, 17, 3),
(23, 18, 4),
(24, 19, 4),
(25, 20, 4),
(26, 21, 2),
(27, 21, 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `ID` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `selector` char(16) DEFAULT NULL,
  `token` char(64) DEFAULT NULL,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `password_reset`
--

INSERT INTO `password_reset` (`ID`, `email`, `selector`, `token`, `expires`) VALUES
(4, 'jane@doe.com', '4526769fa96959e5', '198b0509d236c395b8bb824de0b2eda227620c1905311d63f168df18c0d6838d', 1530236166);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `cars_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `cars_id`, `start_date`, `end_date`) VALUES
(1, 2, '2018-07-17 12:00:00', '2018-07-17 14:00:00'),
(2, 3, '2018-07-18 12:00:00', '2018-07-18 13:00:00'),
(3, 5, '2018-07-18 17:00:00', '2018-07-18 18:00:00'),
(4, 5, '2018-07-19 12:00:00', '2018-07-19 13:00:00'),
(5, 6, '2018-07-19 17:00:00', '2018-07-19 18:00:00'),
(6, 2, '2018-07-20 11:30:00', '2018-07-20 12:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `duration` time NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `price`, `duration`, `description`) VALUES
(1, 'Brakes & Brakes Repair', 75, '01:00:00', NULL),
(2, 'Oil Change', 25, '00:30:00', NULL),
(3, 'Tires & Tires Repair', 35, '00:45:00', NULL),
(4, 'Mufflers & Exhaust Service', 65, '01:15:00', NULL),
(5, 'Belts & Hoses', 45, '00:35:00', NULL),
(6, 'Car Heating & A/C', 55, '00:55:00', NULL),
(7, 'Steering & Suspension', 45, '00:45:00', NULL),
(8, 'Engine', 350, '03:00:00', NULL),
(9, 'Consultation', 0, '00:20:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'pending'),
(2, 'accepted'),
(3, 'declined'),
(4, 'closed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '2',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `active`, `hash`, `role_id`, `created_at`, `expires`) VALUES
(4, 'Customer Black', 'customer@domain.com', '$2y$10$I5dWstv1dK1QrJCQZQY/Hu3K5IqiEhRbMixvgZ9m1qmrP1NmfEWOu', 1, '', 2, '2018-06-27 23:34:14', 0),
(5, 'Miss Jane Doe', 'jane2@doe.com', '$2y$10$F3PvFdk8/8oTYUfU4IIjseKxuk09yvXj/VplWq/1gYSJ4qYCL91N2', 0, '', 2, '2018-06-28 16:07:59', 0),
(6, 'Johnny Doe', 'john1454545@doe.com', '$2y$10$B5Zj0AEn/p519BZ.EAeY8.Iwn593r.5N.RhpvTbSh6W.LEbdgZAQa', 1, '', 1, '2018-06-28 16:45:06', 0),
(13, 'Admin Whitey', 'admin@domain.com', '$2y$10$T4lgRD0Ftwoy817pPlUX1.b2Sw8MXjOOFPW7rreucDd1gSPOkvhq.', 1, '', 1, '2018-07-06 02:10:58', NULL),
(14, 'Atila A', 'atila@atila.com', '$2y$10$ksfds4ypw.DxFmMNmjF3z.eo1S0gg00aLr..vY7IP2MB1u8yrUeh2', 0, '7ce3284b743aefde80ffd9aec500e085', 2, '2018-07-06 11:09:07', 1530911347),
(15, 'Atila A', 'atila@sdsdsd.abcd', '$2y$10$oRTDy2Z49xP4djOuqf4hje/IctBNQ6pc.ni5LbUhyuq5bYbCYUx6K', 0, '0d3180d672e08b4c5312dcdafdf6ef36', 2, '2018-07-06 11:12:53', 1530911573),
(16, 'Atila A', 'thesino@yahoo.com', '$2y$10$aF/KYkMY9JRq8IRbg5KxNetD4j1/dJiyl30aqcyYsg1EWA107vSPe', 1, '274ad4786c3abca69fa097b85867d9a4', 2, '2018-07-11 16:41:00', 1531363260);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_service`
--
ALTER TABLE `job_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `job_service`
--
ALTER TABLE `job_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
