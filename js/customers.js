$(document).ready(function(){

    // Show Modals
   $('#check-status-btn').click(function() {
       $('#check-status-modal').modal('show');
   });

   $('#submit-problem-btn').click(function() {
        $('#submit-problem-modal').modal('show');
   });

   $('#new-vehicle-btn').click(function() {
       $('#new-vehicle-modal').modal('show');
   });

   // Submit Forms Using jQuery and AJAX

   // Search Status Form
   $('#search-status-form').submit(function(event){
       // Get the form data
       var formData = {
        'plate_number'  : $('#plate-number-field').val()
        };
       
        // Proces AJAX request I
        // $.ajax({
        //     type: 'POST',
        //     dataType: 'json',
        //     url: '/customers/process_status.php',
        //     data: formData,
        //     success: function(data) {
        //         $('#results').html(data);
        //         alert('Form submitted successfully.\nReturned json: ' + data);
        //     }
        // });

       // Process AJAX request II
       $.post('/customers/process_status.php', formData, function(data) {
           // Append data into #results div
           $('#results').html(data);
       });
       
       // Prevent default form action
       event.preventDefault();
       $('#check-status-modal').modal('hide');
   });  


   // Plate number to uppercase and strip out nonealphanumeric characters
   // $('#plateNumberInput').focusout(function(){
   //     $(this).val($(this).val().toUpperCase());
   //     $(this).val($(this).val().replace(/[^0-9a-z]/gi, ''));
   // });
});