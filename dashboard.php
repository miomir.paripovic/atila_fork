<?php
    require_once ($_SERVER["DOCUMENT_ROOT"] . '/inc/bootstrap.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/style.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= SITENAME ?></title>
</head>
<body>
    <nav class="navbar navbar-expand-md">
        <a class="navbar-brand" href="#">Admin Area</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <!-- Left side -->
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link" href="/admin/">Dashboard</a>
            </li>
            <li class="nav-item">
            <a class="nav-link disabled" href="/admin/jobs/">Jobs</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/admin/users/">Users</a>
            </li>
            <li class="nav-item">
            <a class="nav-link disabled" href="/admin/cars/">Cars</a>
            </li>
        </ul>
        <!-- Right side -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="/admin/">Welcome, user</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/logout/">Logout</a>
            </li>
        </ul>
        </div>
    </nav>

    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h1><i class="fas fa-cog"></i> Dashboard <small class="text-muted">manage your shop</small></h1>
                </div>
                <div class="col-md-2">
                    <div class="dropdown create">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Create Content
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Add Job</a>
                            <a class="dropdown-item" href="#">Add User</a>
                            <a class="dropdown-item" href="#">Add Car</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>

    <main>
        <div class="container">
            <div class="row">
                <!-- Sidebar -->
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active main-color-bg">
                        <i class="fas fa-cog"></i> Dashboard
                        </a>
                        <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-wrench"></i> Jobs <span class="badge badge-secondary badge-pill float-right">14</span></a>
                        <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-user-circle"></i> Users <span class="badge badge-secondary badge-pill float-right">14</span></a>
                        <a href="#" class="list-group-item list-group-item-action"><i class="fas fa-car"></i> Cars <span class="badge badge-secondary badge-pill float-right">14</span></a>
                    </div>
                </div>
                <div class="col-md-9">
                <!-- Shop Overview -->
                <div class="card mb-4">
                    <div class="card-header main-color-bg">Shop Overview</div>
                        <div class="card-body">
                            <div class="card-deck">
                                <div class="col-md-4">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-wrench"></i> 22</h2>
                                            <h5><strong>Open Jobs</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-id-card-alt"></i> 22</h2>
                                            <h5><strong>Customers</strong></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="card bg-light text-center">
                                        <div class="card-body">
                                            <h2><i class="fas fa-car"></i> 22</h2>
                                            <h5><strong>Cars</strong></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- Latest users -->
                <div class="card">
                    <div class="card-header main-color-bg">Latest Users</div>
                        <div class="card-body">
                            <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">First</th>
                                <th scope="col">Last</th>
                                <th scope="col">Handle</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                </tr>
                                <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                                </tr>
                                <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                                </tr>
                            </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </main>

<div class="container">
<?php require ($_SERVER["DOCUMENT_ROOT"] . '/inc/layout/main/footer.php'); ?>