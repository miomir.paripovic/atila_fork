<?php
    session_start();

    // Instantiate the Flash Message class
    require_once $_SERVER["DOCUMENT_ROOT"] . '/inc/helpers/FlashMessages.php';
    $msg = new \Plasticbrain\FlashMessages\FlashMessages();

// login.php
    function createUserSession($user){
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
        $_SESSION['user_role'] = $user->role_id;
        if($user->role_id === 1) {
            redirect('/admin/');
        } else {
            redirect('/customers/');
        }
    }

    function isLoggedIn() {
        if(isset($_SESSION['user_id'])) {
            return true;
        } else {
            return false;
        }
    }

    function isAdmin() {
        if(isset($_SESSION['user_id']) && $_SESSION['user_role'] === 1) {
            return true;
        } else {
            return false;
        }
    }

    function isUser() {
        if(isset($_SESSION['user_id']) && $_SESSION['user_role'] === 2) {
            return true;
        } else {
            return false;
        }
    }