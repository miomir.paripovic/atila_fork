<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #0099cc;">
    <a class="navbar-brand" href="#">Admin Area</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <!-- Left side -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
        <a class="nav-link" href="/admin/">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/admin/jobs/">Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/admin/users/">Users</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/admin/cars/">Cars</a>
        </li>
    </ul>
    <!-- Right side -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item d-none d-md-block">
        <a class="nav-link" href="/admin/">Welcome, <?= $_SESSION['user_name'] ?></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/logout.php">Logout</a>
        </li>
    </ul>
    </div>
</nav>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h1><i class="fas fa-cog"></i> Dashboard <small class="text-muted"><?= $pageDetails['tagline']; ?></small></h1>
            </div>
            <div class="col-md-2">
                <div class="dropdown create">
                    <button class="btn btn-secondary dropdown-toggle btn-block" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="far fa-file"></i> Create
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/admin/users/jobs/">New Job</a>
                        <a class="dropdown-item" href="/admin/users/add/">New User</a>
                        <a class="dropdown-item" href="/admin/users/cars/">New Car</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<section id="breadcrumb">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Dashboard</li>
                <?php foreach($breadcrumb as $folder) : ?>
                <li class="breadcrumb-item"><?= ucwords($folder) ?></li>
                <?php endforeach; ?>
            </ol>
        </nav>
    </div>
</section>