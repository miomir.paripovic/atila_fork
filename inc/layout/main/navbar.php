<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <div class="container">
        <a class="navbar-brand" href="/"><?= SITENAME ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/about.php">About</a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 1) : ?>
            <li class="nav-item">
            <a class="nav-link" href="/admin/">Admin Area</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/logout.php">Logout</a>
            </li>
            <?php elseif(isset($_SESSION['user_role']) && $_SESSION['user_role'] == 2) : ?>
            <li class="nav-item">
            <a class="nav-link" href="/customers/">Customers Area</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/logout.php">Logout</a>
            </li>
            <?php else : ?>
            <li class="nav-item">
            <a class="nav-link" href="/register.php">Register</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/login.php">Login</a>
            </li>
            <?php endif; ?>
        </ul>
        </div>
    </div>
</nav>