<nav class="navbar navbar-expand-md navbar-dark" style="background-color: #0099cc;">
        <a class="navbar-brand" href="#">Customer Area</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <!-- Left side -->
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
        <a class="nav-link" href="/customers/">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link disabled" href="#">LINK1</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="#">LINK2</a>
        </li>
        <li class="nav-item">
        <a class="nav-link disabled" href="#">LINK3</a>
        </li>
    </ul>
    <!-- Right side -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" href="/customers/">Welcome, <?= $_SESSION['user_name'] ?></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/logout.php">Logout</a>
        </li>
    </ul>
    </div>
</nav>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1><i class="fas fa-car"></i> Dashboard <small class="text-muted"><?= $pageDetails['tagline']; ?></small></h1>
            </div>
        </div>
    </div>
</header>

<section id="breadcrumb">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Dashboard</li>
                <?php foreach($pageDetails['breadcrumb'] as $folder) : ?>
                <li class="breadcrumb-item"><?= $folder ?></li>
                <?php endforeach; ?>
            </ol>
        </nav>
    </div>
</section>