<form class="pr-4 pl-4 needs-validation" id="verify-form" action="/customers/" method="POST" novalidate>    
    <div class="row">
        <div class="col"><input type="button" value="Go to Home Page" class="btn btn-block main-color-bg" onclick="window.location.href='/logout.php'"></div>
        <div class="col"><input type="button" class="btn btn-secondary btn-block" data-dismiss="modal" onclick="window.location.href='/logout.php'" value="Logout"></button></div>
    </div>
</form>